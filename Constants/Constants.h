//
//  Constants.h
//  Constants
//
//  Created by Kukin Valentin on 10/19/18.
//  Copyright © 2018 MobiDev. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for Constants.
FOUNDATION_EXPORT double ConstantsVersionNumber;

//! Project version string for Constants.
FOUNDATION_EXPORT const unsigned char ConstantsVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Constants/PublicHeader.h>


