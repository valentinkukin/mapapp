//
//  Colors.swift
//  Constants
//
//  Created by Kukin Valentin on 10/19/18.
//  Copyright © 2018 MobiDev. All rights reserved.
//

import Foundation
import UIKit

public struct Color {
    
    public static let mainTheme = UIColor(red:0.82, green:0.75, blue:0.00, alpha:1.0)
}
