//
//  Images.swift
//  Constants
//
//  Created by Kukin Valentin on 10/26/18.
//  Copyright © 2018 MobiDev. All rights reserved.
//

import Foundation
import UIKit

public class Images {
    
    public static let favoriteSelected = UIImage(named: "heart-selected.png")
    public static let favoriteDeselected = UIImage(named: "heart-deselected.png")
    public static let deleteAll = UIImage(named: "delete.png")
    public static let placeholder = UIImage(named: "placeholder.png")
    public static let close = UIImage(named: "close.png")
    public static let delete = UIImage(named: "delete-black.png")
    public static let favoritePin = UIImage(named: "favorite-pin.png")
    public static let route = UIImage(named: "route-on.png")
    public static let routeOff = UIImage(named: "route-off.png")
}
