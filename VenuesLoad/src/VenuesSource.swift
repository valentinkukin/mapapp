//
//  VenuesSource.swift
//  VenuesLoad
//
//  Created by Kukin Valentin on 10/29/18.
//  Copyright © 2018 MobiDev. All rights reserved.
//

import Foundation
import CoreLocation
import Foursquer
import Models

public protocol VenueSourceDelegate: class {
    func venueSourceDidLoadVenues(_ venueSource: VenuesSource)
    func venueSource(_ venueSource: VenuesSource, didFailLoadingWithError error: Error)
}

public class VenuesSource {
    
    public internal(set) var venues: [Venue] = []
    
    public weak var delegate: VenueSourceDelegate?
    
    private var radius: Int {
        return 1000
    }
    
    public init() {
        
    }
    
    public func loadVenues(forCoordinate coordinate: CLLocationCoordinate2D) {
        let request = VenueRequestBuilder({return VenueRequest(endPoint: .search, parser: VenueParser.venues)})
            .searchParameters(SearchParameters(coordinate: coordinate, radius: radius))
            .make()
        
        request.send { [weak self] result in
            guard let sSelf = self else {
                return
            }
            sSelf.resultHandler(result)
        }
    }
    
    func resultHandler(_ result: Result<[Venue]>) {
        switch result {
        case let .value(venues):
            self.venues = venues
            self.delegate?.venueSourceDidLoadVenues(self)
        case let .error(error):
            self.delegate?.venueSource(self, didFailLoadingWithError: error)
        }
    }
}
