//
//  VenueDetail.swift
//  Models
//
//  Created by Kukin Valentin on 10/24/18.
//  Copyright © 2018 MobiDev. All rights reserved.
//

import Foundation

public struct VenueDetail {
    public let description: String
    public let imageURL: String
    
    public init(description: String, imageURL: String) {
        self.description = description
        self.imageURL = imageURL
    }
}
