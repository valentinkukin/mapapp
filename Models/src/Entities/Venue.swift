//
//  Vanue.swift
//  Models
//
//  Created by Kukin Valentin on 10/17/18.
//  Copyright © 2018 MobiDev. All rights reserved.
//

import Foundation
import CoreLocation
import CoreData

public class Venue: NSManagedObject {
    @NSManaged public var id: String
    @NSManaged public var name: String
    @NSManaged public var category: String
    @NSManaged public var latitude: Double
    @NSManaged public var longitude: Double
    @NSManaged public var venueDescription: String?
    @NSManaged public var imageURL: String?
}

extension Venue {
    
    public var coordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    
    public func copy(_ venue: Venue) {
        id = venue.id
        name = venue.name
        category = venue.category
        latitude = venue.latitude
        longitude = venue.longitude
        venueDescription = venue.venueDescription
        imageURL = venue.imageURL
    }
}
