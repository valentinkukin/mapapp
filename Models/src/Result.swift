//
//  Result.swift
//  Models
//
//  Created by Kukin Valentin on 10/24/18.
//  Copyright © 2018 MobiDev. All rights reserved.
//

import Foundation

public enum Result<Value> {
    case value(Value)
    case error(Error)
}
