//
//  VenueRequestBuilder.swift
//  Foursquer
//
//  Created by Kukin Valentin on 10/24/18.
//  Copyright © 2018 MobiDev. All rights reserved.
//

import Foundation
import CoreLocation
import Alamofire

public struct SearchParameters {
    let latitude: Double
    let longitude: Double
    let radius: Int
    
    public init(coordinate: CLLocationCoordinate2D, radius: Int) {
        self.latitude = coordinate.latitude
        self.longitude = coordinate.longitude
        self.radius = radius
    }
}

public class VenueRequestBuilder<Value> {
    
    private let venueRequest: VenueRequest<Value>
    
    public init(_ fractoryMethod: () -> VenueRequest<Value>) {
        venueRequest = fractoryMethod()
    }
    
    @discardableResult
    public func searchParameters(_ params: SearchParameters) -> VenueRequestBuilder<Value> {
        venueRequest.parameters.updateValue("\(params.latitude),\(params.longitude)", forKey: ParameterName.ll)
        venueRequest.parameters.updateValue(params.radius, forKey: ParameterName.radius)
        return self
    }
    
    public func make() -> VenueRequest<Value> {
        return venueRequest
    }
}
