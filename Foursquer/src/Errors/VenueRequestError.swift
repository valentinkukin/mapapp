//
//  VanueRequestError.swift
//  Foursquer
//
//  Created by Kukin Valentin on 10/17/18.
//  Copyright © 2018 MobiDev. All rights reserved.
//

import Foundation

public enum VenueRequestError: LocalizedError {
    
    case connectionError
    case invalidServerResponse

    public var errorDescription: String? {
        switch self {
        case .connectionError:
            return "No internet connection"
        case .invalidServerResponse:
            return "Server is not responding."
        }
    }
}
