//
//  Parser.swift
//  Foursquer
//
//  Created by Kukin Valentin on 10/24/18.
//  Copyright © 2018 MobiDev. All rights reserved.
//

import Foundation
import Models
import FavoritiesVenuesCoreData

public struct VenueParser {
    
    public static func venueDetail(from json: Any) -> VenueDetail? {
        guard let data = json as? [String: Any], let response = data[.response] as? [String: Any] else {
            return nil
        }
        guard let venueInfo = response[.venue] as? [String: Any] else {
            return nil
        }
        var description: String?, photoURL: String?
        description = venueInfo[.description] as? String
        if let photoInfo = venueInfo[.bestPhoto] as? [String: Any] {
            if let prefix = photoInfo[.prefix] as? String,
                let suffix = photoInfo[.suffix] as? String {
                photoURL = prefix + "256x256" + suffix
            }
        }
        return VenueDetail(description:description ?? "No information", imageURL: photoURL ?? "")
    }
    
    public static func venues(from json: Any) -> [Venue]? {
        guard let data = json as? [String: Any], let response = data[.response] as? [String: Any] else {
            return nil
        }
        guard let venuesInfoList = response[.venues] as? [[String: Any]] else {
            return nil
        }
        var venues: [Venue] = []
        for venueInfo in venuesInfoList {
            guard let venue = parsedVenue(from: venueInfo) else {
                return nil
            }
            venues.append(venue)
        }
        return venues
    }
    
    private static func parsedVenue(from venueInfo: [String: Any]) -> Venue? {
        guard let id = venueInfo[.id] as? String else {
            return nil
        }
        guard let location = venueInfo[.location] as? [String: Any],
            let lat = location[.latitude] as? Double,
            let lng = location[.longitude] as? Double else {
                return nil
        }
        guard let name = venueInfo[.name] as? String else {
            return nil
        }
        var category: String?
        if let categories = venueInfo[.categories] as? [[String: Any]],
            let categoryName = categories.first?[.name] as? String {
            category = categoryName
        }
        return CoreDataStack.defaultStack.createVenue(id: id, name: name, category: category ?? "", latitude: lat, longitude: lng)
    }
}
