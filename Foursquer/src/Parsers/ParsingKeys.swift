//
//  ParsingKeys.swift
//  Foursquer
//
//  Created by Kukin Valentin on 10/23/18.
//  Copyright © 2018 MobiDev. All rights reserved.
//

import Foundation

enum VenueParsingKeys: String {
    
    case id = "id"
    case response = "response"
    case venues = "venues"
    case name = "name"
    case categories = "categories"
    case location = "location"
    case latitude = "lat"
    case longitude = "lng"
    case distance = "distance"
    case venue = "venue"
    case description = "description"
    case bestPhoto = "bestPhoto"
    case prefix = "prefix"
    case suffix = "suffix"
    case width = "width"
    case height = "height"
}

extension Dictionary where Key == String, Value == Any {
    subscript(key: VenueParsingKeys) -> Value? {
        return self[key.rawValue]
    }
}
extension Dictionary where Key == String, Value == String {
    subscript(key: VenueParsingKeys) -> Value? {
        return self[key.rawValue]
    }
}
