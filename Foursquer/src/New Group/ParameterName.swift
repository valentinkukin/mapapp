//
//  ParametersName.swift
//  Foursquer
//
//  Created by Kukin Valentin on 10/23/18.
//  Copyright © 2018 MobiDev. All rights reserved.
//

import Foundation

struct ParameterName {
    
    static let clientId = "client_id"
    static let clientSecret = "client_secret"
    static let version = "v"
    static let intent = "intent"
    static let categoryId = "categoryId"
    static let ll = "ll"
    static let radius = "radius"
}
