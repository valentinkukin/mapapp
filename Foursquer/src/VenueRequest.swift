//
//  VenueRequest.swift
//  Foursquer
//
//  Created by Kukin Valentin on 10/24/18.
//  Copyright © 2018 MobiDev. All rights reserved.
//

import Foundation
import Alamofire
import Models

let kChekinValue = "checkin"
let kCategoryValue = "4d4b7105d754a06374d81259"

public class VenueRequest<Value> {
    
    private let endPoint: VenueRequestEndPoint
    private let parser: (Any)->Value?
    var parameters: Parameters = [
        ParameterName.clientId: FoursquerAccess.clientId,
        ParameterName.clientSecret: FoursquerAccess.clientSecret,
        ParameterName.version: FoursquerAccess.version,]
    
    public init(endPoint: VenueRequestEndPoint, parser: @escaping (Any)->Value?) {
        self.parser = parser
        self.endPoint = endPoint
        serExtraParametersIfNeeded()
    }
    
    private func serExtraParametersIfNeeded() {
        switch endPoint {
        case .search:
            parameters.updateValue(kChekinValue, forKey: ParameterName.intent)
            parameters.updateValue(kCategoryValue, forKey: ParameterName.categoryId)
        case .details:
            break
        }
    }
    
    public func send(completion: @escaping (Models.Result<Value>) -> Void) {
        let requestQueue = DispatchQueue(label: "fourquer_details_queue")
        Alamofire
            .request(endPoint.url, parameters: parameters)
            .validate()
            .responseJSON(queue: requestQueue) { response in
                switch response.result {
                case let .success(json):
                    guard let value = self.parser(json) else {
                        DispatchQueue.main.async {
                            completion(.error(VenueRequestError.invalidServerResponse))
                        }
                        return
                    }
                    DispatchQueue.main.async {
                        completion(.value(value))
                    }
                case .failure:
                    DispatchQueue.main.async {
                        completion(.error(VenueRequestError.connectionError))
                    }
                }
        }
    }
}

public enum VenueRequestEndPoint {
    case search
    case details(String)
    
    var url: String {
        switch self {
        case .search:
            return FoursquerAccess.url + VenueRequestEndPoint.searchURLPreffix
        case let .details(venueId):
            return FoursquerAccess.url + venueId
        }
    }
    
    private static let searchURLPreffix =  "search"
}
