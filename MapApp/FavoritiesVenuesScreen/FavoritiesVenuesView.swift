//
//  FavoritiesVenuesView.swift
//  MapApp
//
//  Created by Kukin Valentin on 10/25/18.
//  Copyright © 2018 MobiDev. All rights reserved.
//

import UIKit
import Constants

class FavoritiesVenuesView: UIView {
    
    let tableView = UITableView()
    let deleteButton = UIButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeTableView()
        iniializeDeleteButton()
        let views: [UIView] = [tableView, deleteButton]
        views.forEach(addSubview)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initializeTableView() {
        tableView.tableFooterView = UIView()
        tableView.register(FavoriteVenueCell.self, forCellReuseIdentifier: FavoriteVenueCell.reuseID)
        tableView.separatorStyle = .none
    }
    
    private func iniializeDeleteButton() {
        deleteButton.setImage(Images.deleteAll, for: .normal)
        deleteButton.backgroundColor = .red
        deleteButton.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        deleteButton.layer.cornerRadius = 15
        deleteButton.isHidden = true
    }
    
    override func layoutSubviews() {
        updateTableViewFrame()
        updateDeleteButtonFrame()
    }
    
    private func updateTableViewFrame() {
        tableView.frame = bounds
    }
    
    private func updateDeleteButtonFrame() {
        let side: CGFloat = 50
        let padding: CGFloat = 10
        deleteButton.frame = CGRect(
            x: bounds.maxX - padding - side, y: bounds.maxY - padding - side, width: side, height: side)
    }
}

extension UITableViewCell {
    static var reuseID: String {
        return NSStringFromClass(self)
    }
}
