//
//  FavoriteVenueCellTableViewCell.swift
//  MapApp
//
//  Created by Kukin Valentin on 10/25/18.
//  Copyright © 2018 MobiDev. All rights reserved.
//

import UIKit
import Constants

class FavoriteVenueCell: UITableViewCell {

    let nameLabel = UILabel()
    let categoryLabel = UILabel()
    let textView = UITextView()
    let venueImageView = UIImageView()
    
    private var padding: CGFloat {
        return 5
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        inintializeLabels()
        initializeTextView()
        initializeImageView()
        let views: [UIView] = [nameLabel, categoryLabel, textView, venueImageView]
        views.forEach(addSubview)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func inintializeLabels() {
        nameLabel.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight(1))
        categoryLabel.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight(0.1))
    }
    
    func initializeTextView() {
        textView.isScrollEnabled = true
        textView.isEditable = false
    }
    
    func initializeImageView() {
        venueImageView.contentMode = .scaleAspectFit
        venueImageView.layer.borderColor = Color.mainTheme.cgColor
        venueImageView.layer.borderWidth = 1
    }

    override func layoutSubviews() {
        updateImageViewFrame()
        updateLabelFrames()
        updateTextViewFrame()
    }
    
    func updateImageViewFrame() {
        let side = bounds.height - padding*2
        venueImageView.frame = CGRect(x: bounds.maxX - padding - side, y: padding, width: side, height: side)
    }
    
    func updateLabelFrames() {
        nameLabel.frame = CGRect(x: padding, y: padding, width: venueImageView.frame.minX - padding*2, height: 25)
        categoryLabel.frame = CGRect(x: padding, y: nameLabel.frame.maxY, width: nameLabel.frame.width, height: 10)
    }
    
    func updateTextViewFrame() {
        let y = categoryLabel.frame.maxY
        textView.frame = CGRect(
            x: 0, y: y + padding, width: nameLabel.frame.width + padding, height: bounds.maxY - padding*2 - y)
    }
}
