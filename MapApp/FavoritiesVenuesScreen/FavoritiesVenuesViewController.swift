//
//  FavoritiesVenuesViewController.swift
//  MapApp
//
//  Created by Kukin Valentin on 10/25/18.
//  Copyright © 2018 MobiDev. All rights reserved.
//

import UIKit
import Constants
import Models
import AlamofireImage
import CoreData
import FavoritiesVenuesCoreData

class FavoritiesVenuesViewController: UIViewController {

    private let fetchedResultsController = CoreDataStack.defaultStack.fetchedResultsController
    private var favoritiesView: FavoritiesVenuesView? {
        return view as? FavoritiesVenuesView
    }
    
    override func loadView() {
        view = FavoritiesVenuesView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeNavigationBar()
        favoritiesView?.deleteButton.addTarget(self, action: #selector(handleDelete(_:)), for: .touchUpInside)
        favoritiesView?.tableView.delegate = self
        favoritiesView?.tableView.dataSource = self
        setupFetchedResultController()
        toggleDeleteButton()
    }
    
    private func setupFetchedResultController() {
        fetchedResultsController.delegate = self
    }
    
    @objc
    private func handleDelete(_ sender: UIButton) {
        showDeleteAllAlert()
    }
    
    private func showDeleteAllAlert() {
        let alert = UIAlertController(title: "Delete All", message: "Do you want to delete all favorite venues?",
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in return }))
        alert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { _ in
            self.deleteAllFavorities()
        }))
        present(alert, animated: true, completion: nil)
    }
    
    private func toggleDeleteButton() {
        favoritiesView?.deleteButton.isHidden = fetchedResultsController.fetchedObjects?.isEmpty ?? true
    }
    
    private func deleteAllFavorities() {
        guard let venues = fetchedResultsController.fetchedObjects else {
            return
        }
        for venue in venues {
            CoreDataStack.defaultStack.delete(venue)
        }
    }
}

extension FavoritiesVenuesViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return fetchedResultsController.sections?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchedResultsController.sections?[section].numberOfObjects ?? 0
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return fetchedResultsController.sections?[section].indexTitle ?? ""
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: FavoriteVenueCell.reuseID, for: indexPath) as! FavoriteVenueCell
        let venue = fetchedResultsController.object(at: indexPath)
        update(cell, withVenue: venue)
        return cell
    }
    
    private func update(_ cell: FavoriteVenueCell, withVenue venue: Venue) {
        cell.nameLabel.text = venue.name
        cell.categoryLabel.text = venue.category
        cell.textView.text = venue.venueDescription
        if let imageURL = venue.imageURL, let url = URL(string: imageURL) {
            cell.venueImageView.af_setImage(withURL: url, placeholderImage: Images.placeholder)
        }
    }
}

extension FavoritiesVenuesViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            CoreDataStack.defaultStack.delete(fetchedResultsController.object(at: indexPath))
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let venue = fetchedResultsController.object(at: indexPath)
        let presenter = FavoriteMapPresenter(venue: venue)
        let vc = MapViewController(presenter: presenter)
        navigationController?.pushViewController(vc, animated: true)
    }
}

private extension FavoritiesVenuesViewController {
    
    func initializeNavigationBar() {
        navigationItem.title = "Favorite"
    }
}

extension FavoritiesVenuesViewController: NSFetchedResultsControllerDelegate {
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        favoritiesView?.tableView.beginUpdates()
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        favoritiesView?.tableView.endUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>,
                    didChange anObject: Any,
                    at indexPath: IndexPath?,
                    for type: NSFetchedResultsChangeType,
                    newIndexPath: IndexPath?) {
        if type == .delete, let indexPath = indexPath {
            toggleDeleteButton()
            favoritiesView?.tableView.deleteRows(at: [indexPath], with: .fade)
            if controller.fetchedObjects?.isEmpty ?? true {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>,
                    didChange sectionInfo: NSFetchedResultsSectionInfo,
                    atSectionIndex sectionIndex: Int,
                    for type: NSFetchedResultsChangeType) {
        if type == .delete {
            favoritiesView?.tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        }
    }
}
