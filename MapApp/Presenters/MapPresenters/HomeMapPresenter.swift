//
//  SearchVenuesPresenter.swift
//  MapApp
//
//  Created by Kukin Valentin on 10/30/18.
//  Copyright © 2018 MobiDev. All rights reserved.
//

import Foundation
import Models
import VenuesLoad
import FavoritiesVenuesCoreData
import Constants
import MapKit

class HomeMapPresenter: MapPresenter {
    
    let viewModel: MapViewModel
    private let venueSource = VenuesSource()
    private var location = CLLocation()
    private let fetchedResultController = CoreDataStack.defaultStack.fetchedResultsController
    
    weak var mapView: MKMapView?
    weak var delegate: MapPresenterDelegate?
    
    var hideVenueDetailsWhenTapOnActionButton: Bool {
        return false 
    }
    
    init() {
        viewModel = MapViewModel(title: "Venues",
                                 canShowUserLocation: true,
                                 canShowSelectedLocation: true,
                                 rightBarButtonIcon: Images.favoriteSelected)
        venueSource.delegate = self
    }
    
    func navigationAction(navigationController: UINavigationController?) {
        let favoritiesVC = FavoritiesVenuesViewController()
        navigationController?.pushViewController(favoritiesVC, animated: true)
    }
    
    func venueDetailsAction(_ venueDetailsViewController: VenueDetailsViewController) {
        let venue = venueDetailsViewController.venue
        if isFavorite(venue) {
            venueDetailsViewController.setActionButtonIcon(Images.favoriteDeselected)
            if let favoriteVenue = fetchedResultController.fetchedObjects?.first(where: { $0.id == venue.id }) {
                CoreDataStack.defaultStack.delete(favoriteVenue)
            }
        } else {
            venueDetailsViewController.setActionButtonIcon(Images.favoriteSelected)
            CoreDataStack.defaultStack.save(venueDetailsViewController.venue)
        }
    }
    
    private func isFavorite(_ venue: Venue) -> Bool {
        return fetchedResultController.fetchedObjects?.contains { $0.id == venue.id } ?? false
    }
    
    func setLocation(_ location: CLLocation) {
        self.location = location
        updateMapViewAnnotations()
        if location == mapView?.userLocation.location {
            setMapRegion(for: location.coordinate)
            mapView?.showsUserLocation = false
        }
    }
    
    private func updateMapViewAnnotations() {
        clearMapAnnotations()
        requestNewVenues()
        displayCurrentLocationAnnotation()
    }
}

extension HomeMapPresenter {
    
    private func clearMapAnnotations() {
        guard let mapView = mapView else {
            return
        }
        mapView.removeAnnotations(mapView.annotations)
    }
    
    private func requestNewVenues() {
        CoreDataStack.defaultStack.clearAllInMemoryVenues()
        venueSource.loadVenues(forCoordinate: location.coordinate)
    }
    
    private func displayCurrentLocationAnnotation() {
        mapView?.addAnnotation(Annotation(title: "You", subtitle: "",coordinate: location.coordinate))
    }
}

extension HomeMapPresenter: VenueSourceDelegate {
    
    func venueSourceDidLoadVenues(_ venueSource: VenuesSource) {
        let annotations = venueSource.venues.map(Annotation.init(venue:))
        mapView?.addAnnotations(annotations)
    }
    
    func venueSource(_ venueSource: VenuesSource, didFailLoadingWithError error: Error) {
        delegate?.mapPresenter(self, didHandleError: error)
    }
}

extension HomeMapPresenter {
    
    func venueDetailActionButtonIcon(for venue: Venue) -> UIImage? {
        if isFavorite(venue) {
            return Images.favoriteSelected
        } else {
            return Images.favoriteDeselected
        }
    }
    
    func viewForAnnotation(_ annotation: Annotation, reuseId id: String) -> MKAnnotationView {
        let view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: id)
        if annotation.venue != nil {
            view.canShowCallout = true
            view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            view.calloutOffset = CGPoint(x: 0, y: 5)
        }
        return view
    }
}
