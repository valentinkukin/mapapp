//
//  FavoriteVenueMapPresenter.swift
//  MapApp
//
//  Created by Kukin Valentin on 10/31/18.
//  Copyright © 2018 MobiDev. All rights reserved.
//

import Foundation
import MapKit
import FavoritiesVenuesCoreData
import Models
import CoreLocation
import Constants

class FavoriteMapPresenter: MapPresenter {
    
    let viewModel: MapViewModel
    private var location = CLLocation()
    private let venue: Venue
    
    weak var delegate: MapPresenterDelegate?
    weak var mapView: MKMapView? {
        didSet {
            displayVenueAnnotation()
        }
    }
    
    private var route: MKRoute? {
        didSet {
            displayRoute()
            if route == nil {
                clearMapOverlays()
            }
        }
    }
    
    var hideVenueDetailsWhenTapOnActionButton: Bool {
        return true
    }
    
    init(venue: Venue) {
        viewModel = MapViewModel(title: venue.name,
                                 canShowUserLocation: false,
                                 canShowSelectedLocation: false,
                                 rightBarButtonIcon: Images.delete)
        self.venue = venue
    }
    
    func navigationAction(navigationController: UINavigationController?) {
        CoreDataStack.defaultStack.delete(venue)
        navigationController?.popViewController(animated: true)
    }
    
    func venueDetailsAction(_ venueDetailsViewController: VenueDetailsViewController) {
        if route == nil {
            delegate?.mapPresenterWillRequestUserLocation(self)
            mapView?.showsUserLocation = true
        } else {
            mapView?.showsUserLocation = false
            route = nil
        }
    }
    
    private func displayVenueAnnotation() {
        mapView?.addAnnotation(Annotation(venue: venue))
        setMapRegion(for: venue.coordinate)
    }
    
    private func displayRoute() {
        guard let route = route else {
            return
        }
        mapView?.addOverlay(route.polyline, level: .aboveRoads)
        let rect = route.polyline.boundingMapRect
        let edgePadding = UIEdgeInsets(top: 100, left: 100, bottom: 100, right: 100)
        mapView?.setVisibleMapRect(rect, edgePadding: edgePadding,  animated: true)
    }
    
    private func clearMapOverlays() {
        guard let mapView = mapView else {
            return
        }
        mapView.removeOverlays(mapView.overlays)
    }
    
    func setLocation(_ location: CLLocation) {
        if location == mapView?.userLocation.location {
            self.location = location
            showRouteToVenue()
        }
    }
    
    private func showRouteToVenue() {
        guard let directionRequest = makeDirectionRequest() else {
            return
        }
        let directions = MKDirections(request: directionRequest)
        setRoute(for: directions)
    }
    
    private func makeDirectionRequest() -> MKDirections.Request? {
        let directionRequest = MKDirections.Request()
        directionRequest.source = mapItem(for: location.coordinate)
        directionRequest.destination = mapItem(for: venue.coordinate)
        directionRequest.transportType = .walking
        return directionRequest
    }
    
    private func mapItem(for coordinates: CLLocationCoordinate2D) -> MKMapItem {
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        return MKMapItem(placemark: placemark)
    }
    
    private func setRoute(for directions: MKDirections) {
        directions.calculate { [weak self] response, error -> Void in
            guard let sSelf = self else {
                return
            }
            if let error = error {
                sSelf.delegate?.mapPresenter(sSelf, didHandleError: error)
                return
            }
            if let route = response?.routes.first {
                sSelf.route = route
            }
        }
    }
}

extension FavoriteMapPresenter {
    
    func venueDetailActionButtonIcon(for venue: Venue) -> UIImage? {
        if route == nil {
            return Images.route
        } else {
            return Images.routeOff
        }
    }
    
    func viewForAnnotation(_ annotation: Annotation, reuseId id: String) -> MKAnnotationView {
        let pinView = MKAnnotationView(annotation: annotation, reuseIdentifier: id)
        pinView.image = Images.favoritePin
        pinView.canShowCallout = true
        pinView.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        return pinView
    }
}
