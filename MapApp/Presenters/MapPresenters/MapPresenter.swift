//
//  MapViewControllerPresentor.swift
//  MapApp
//
//  Created by Kukin Valentin on 10/30/18.
//  Copyright © 2018 MobiDev. All rights reserved.
//

import Foundation
import MapKit
import CoreLocation
import Models

protocol MapPresenterDelegate: class {
    func mapPresenterWillRequestUserLocation(_ presenter: MapPresenter)
    func mapPresenter(_ presenter: MapPresenter, didHandleError error: Error)
}

protocol MapPresenter {
    
    var viewModel: MapViewModel { get }
    var hideVenueDetailsWhenTapOnActionButton: Bool { get }
    var mapView: MKMapView? { get set }
    var delegate: MapPresenterDelegate? { get set }
    
    func setLocation(_ location: CLLocation)
    func navigationAction(navigationController: UINavigationController?)
    func venueDetailsAction(_ venueDetailsViewController: VenueDetailsViewController)
    func venueDetailActionButtonIcon(for venue: Venue) -> UIImage?
    func viewForAnnotation(_ annotation: Annotation, reuseId id: String) -> MKAnnotationView
}

extension MapPresenter {
    
    func setMapRegion(for coordinate: CLLocationCoordinate2D) {
        let delta = 1e-3 * 2
        let span = MKCoordinateSpan(latitudeDelta: delta, longitudeDelta: delta)
        let region = MKCoordinateRegion(center: coordinate, span: span)
        mapView?.setRegion(region, animated: true)
    }
}
