//
//  AppDelegate.swift
//  MapApp
//
//  Created by Kukin Valentin on 10/17/18.
//  Copyright © 2018 MobiDev. All rights reserved.
//

import UIKit
import Constants

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        createWindow()
        customizeNavigationBarAppearance()
        return true
    }
    
    private func createWindow() {
        let window = UIWindow(frame: UIScreen.main.bounds)
        let presenter = HomeMapPresenter()
        let vc = MapViewController(presenter: presenter)
        let navigationController = UINavigationController(rootViewController: vc)
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
        self.window = window
    }
    
    private func customizeNavigationBarAppearance() {
        UINavigationBar.appearance().barTintColor = Color.mainTheme
        UINavigationBar.appearance().tintColor = .black
    }
}
