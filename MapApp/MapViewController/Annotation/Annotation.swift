//
//  Annotation.swift
//  MapApp
//
//  Created by Kukin Valentin on 10/19/18.
//  Copyright © 2018 MobiDev. All rights reserved.
//

import Foundation
import MapKit
import Models

class Annotation: NSObject, MKAnnotation {
    let title: String?
    let subtitle: String?
    let coordinate: CLLocationCoordinate2D
    let venue: Venue?
    
    init(venue: Venue) {
        self.venue = venue
        self.title = venue.name
        self.subtitle = venue.category
        self.coordinate = venue.coordinate
        super.init()
    }
    
    init(title: String?, subtitle: String?, coordinate: CLLocationCoordinate2D, id: String? = nil) {
        self.venue = nil
        self.title = title
        self.subtitle = subtitle
        self.coordinate = coordinate
    }
}
