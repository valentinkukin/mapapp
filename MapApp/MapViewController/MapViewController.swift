//
//  MapPresentorViewController.swift
//  MapApp
//
//  Created by Kukin Valentin on 10/30/18.
//  Copyright © 2018 MobiDev. All rights reserved.
//

import UIKit
import MapKit
import Models

class MapViewController: UIViewController {

    private var presenter: MapPresenter
    private let locationManager = CLLocationManager()
    private var venueDetailsVC: VenueDetailsViewController?
    private var mapScreenView: MapScreenView? {
        return view as? MapScreenView
    }
    
    init(presenter: MapPresenter) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }
    
    override func loadView() {
        view = MapScreenView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapScreenView?.mapView.delegate = self
        presenter.mapView = mapScreenView?.mapView
        presenter.delegate = self
        initializeNavigationBar()
        updateCurrentLocationButton()
        if presenter.viewModel.canShowSelectedLocation {
            addLongPressGesture()
        }
    }
    
    private func updateCurrentLocationButton() {
        mapScreenView?.currentLocationButton.isHidden = !presenter.viewModel.canShowUserLocation
        mapScreenView?.currentLocationButton.addTarget(self, action: #selector(handleCurrentLocation(_:)), for: .touchUpInside)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc
    private func handleCurrentLocation(_ sender: UIButton) {
        requestLocationAuthorization()
        mapScreenView?.mapView.showsUserLocation = true
    }
}

//MARK: - Initializing Navigation Bar

extension MapViewController {
    
    private func initializeNavigationBar() {
        navigationItem.title = presenter.viewModel.title
        let rightButtonItem = UIBarButtonItem(image: presenter.viewModel.rightBarButtonIcon, style: .plain, target: self,
                                               action: #selector(handleRightBarButtonAction(_:)))
        navigationItem.rightBarButtonItem = rightButtonItem
    }
    
    @objc
    private func handleRightBarButtonAction(_ sender: UIButton) {
        presenter.navigationAction(navigationController: navigationController)
    }
}

//MARK: - Long Press Gesure

extension MapViewController {

    private func addLongPressGesture() {
        let longPress = UILongPressGestureRecognizer()
        longPress.addTarget(self, action: #selector(handleLongPress(_:)))
        mapScreenView?.mapView.addGestureRecognizer(longPress)
    }

    @objc func handleLongPress(_ longPress: UILongPressGestureRecognizer) {
        if longPress.state == .began {
            let point = longPress.location(in: mapScreenView?.mapView)
            guard let location = convertedLocation(from: point) else {
                return
            }
            presenter.setLocation(location)
        }
    }
    
    private func convertedLocation(from point: CGPoint) -> CLLocation? {
        guard let map = mapScreenView?.mapView else {
            return nil
        }
        let coordinate = map.convert(point, toCoordinateFrom: map)
        return CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
    }
}

//MARK: - Map View Delegate

extension MapViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        guard let location = userLocation.location else {
            return
        }
        presenter.setLocation(location)
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let annotation = annotation as? Annotation, annotation.venue != nil else {
            return nil
        }
        let id = "id"
        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: id) as? MKMarkerAnnotationView {
            dequeuedView.annotation = annotation
            return dequeuedView
        }
        return presenter.viewForAnnotation(annotation, reuseId: id)
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        guard let annotation = view.annotation as? Annotation, let venue = annotation.venue else {
            return
        }
        if control === view.rightCalloutAccessoryView {
            showVenueDetailsController(venue: venue)
            centerIfHided(annotation)
        }
    }
    
    private func showVenueDetailsController(venue: Venue) {
        guard venue.id != self.venueDetailsVC?.venue.id else {
            return
        }
        dismissVenueDetails()
        initializeVenueDetailsVC(with: venue)
        displayVenueDetails()
    }
    
    private func initializeVenueDetailsVC(with venue: Venue) {
        let actionButtonIcon = presenter.venueDetailActionButtonIcon(for: venue)
        let venueDetailsVC = VenueDetailsViewController(venue: venue, actionButtonIcon: actionButtonIcon)
        venueDetailsVC.delegate = self
        self.venueDetailsVC = venueDetailsVC
    }
    
    private func centerIfHided(_ annotation: Annotation) {
        let annotationPoint = mapScreenView?.mapView.convert(annotation.coordinate, toPointTo: nil) ?? .zero
        if venueDetailsVC?.view.frame.contains(annotationPoint) ?? false {
            mapScreenView?.mapView.setCenter(annotation.coordinate, animated: true)
        }
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = .blue
        renderer.lineWidth = 4.0
        return renderer
    }
}

extension MapViewController {
    
    private func displayVenueDetails() {
        guard let detailsVC = venueDetailsVC, let detailsView = detailsVC.view else {
            return
        }
        mapScreenView?.venueDetailsView = detailsView
        let height: CGFloat = 235
        detailsView.frame = CGRect(x: 0, y: view.frame.maxY - height, width: view.frame.width, height: height)
        view.addSubview(detailsView)
        addChild(detailsVC)
        detailsView.frame.origin.y += detailsView.frame.height
        UIView.animate(withDuration: 0.3,animations: {
            detailsView.frame.origin.y -= detailsView.frame.height
            self.view.layoutSubviews()
        })
        didMove(toParent: venueDetailsVC)
    }
    
    func dismissVenueDetails() {
        guard let venueDetailsVC = self.venueDetailsVC else {
            return
        }
        UIView.animate(withDuration: 0.3,animations: {
            venueDetailsVC.view.frame.origin.y += venueDetailsVC.view.frame.height
            self.view.layoutSubviews()
        }, completion: { _ in
            venueDetailsVC.willMove(toParent: nil)
            venueDetailsVC.view.removeFromSuperview()
            venueDetailsVC.removeFromParent()
        })
        self.venueDetailsVC = nil
    }
}

extension MapViewController: VenueDetailsViewControllerDelegate {
    
    func venueDetailsViewControllerDidTapOnActionButton(_ viewController: VenueDetailsViewController) {
        presenter.venueDetailsAction(viewController)
        if presenter.hideVenueDetailsWhenTapOnActionButton {
            dismissVenueDetails()
        }
    }
    
    func venueDetailsViewControllerDidTapOnCloseButton(_ viewController: VenueDetailsViewController) {
        dismissVenueDetails()
    }
}


extension MapViewController: MapPresenterDelegate {
    
    func mapPresenter(_ presenter: MapPresenter, didHandleError error: Error) {
        showAlert(title: "Error",
                  message: error.localizedDescription,
                  cancel: "Ок")
    }
    
    func mapPresenterWillRequestUserLocation(_ presenter: MapPresenter) {
        requestLocationAuthorization()
    }
    
    func requestLocationAuthorization() {
        self.locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.authorizationStatus() == .denied {
            showAlert(title: "Location Access Denied!",
                      message: "You have denied access to geolocation service on your device",
                      cancel: "Cancel",
                      settings: "Open Settings")
        }
    }
    
    private func showAlert(title: String, message: String, cancel: String, settings: String? = nil) {
        let alert = UIAlertController( title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: cancel, style: .cancel) { _ in
            return
        })
        if let settingsText = settings {
            alert.addAction(UIAlertAction(title: settingsText, style: .default) { _ in
                guard let url = URL(string: UIApplication.openSettingsURLString) else {
                    return
                }
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            })
        }
        present(alert, animated: true, completion: nil)
    }
}
