//
//  MainScreenView.swift
//  MapApp
//
//  Created by Kukin Valentin on 10/17/18.
//  Copyright © 2018 MobiDev. All rights reserved.
//

import UIKit
import MapKit
import Constants

class MapScreenView: UIView {
    
    let mapView = MKMapView()
    let currentLocationButton = UIButton()
    weak var venueDetailsView: UIView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        initializeMapView()
        initializeCurrentLocationButton()
        let views: [UIView] = [mapView, currentLocationButton]
        views.forEach(addSubview)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initializeMapView() {
        mapView.mapType = .standard
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
    }
    
    private func initializeCurrentLocationButton() {
        currentLocationButton.setTitle("My location", for: .normal)
        currentLocationButton.setTitleColor(Color.mainTheme, for: .normal)
        currentLocationButton.setTitleColor(Color.mainTheme.withAlphaComponent(0.5), for: .highlighted)
        currentLocationButton.backgroundColor = .black
        currentLocationButton.layer.borderWidth = 2
        currentLocationButton.layer.borderColor = Color.mainTheme.cgColor
    }
    
    override func layoutSubviews() {
        updateMapViewFrame()
        updateCurrentLocationButton()
    }
    
    private func updateMapViewFrame() {
        mapView.frame = bounds
    }
    
    private func updateCurrentLocationButton() {
        let padding: CGFloat = 10
        let width: CGFloat = 110
        let height: CGFloat = 30
        let y = (venueDetailsView?.frame.minY ?? bounds.maxY)
        currentLocationButton.frame = CGRect(
            x: bounds.maxX - width - padding, y: y - height - padding, width: width, height: height)
    }
}
