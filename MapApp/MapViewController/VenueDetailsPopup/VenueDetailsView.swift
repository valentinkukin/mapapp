//
//  VenueDetailsView.swift
//  MapApp
//
//  Created by Kukin Valentin on 10/22/18.
//  Copyright © 2018 MobiDev. All rights reserved.
//

import Foundation
import UIKit
import Constants

class VenueDetailsView: UIView {
    
    let nameLabel = UILabel()
    let categoryLabel = UILabel()
    let closeButton = UIButton()
    let textView = UITextView()
    let imageView = UIImageView()
    let actionButton = UIButton()
    let errorView = VenueDetailsErrorView()
    
    private var padding: CGFloat {
        return 5
    }
    
    init() {
        super.init(frame: .zero)
        initializeView()
        inintializeLabels()
        initializeCloseButton()
        initializeTextView()
        initializeImageView()
        initializeActionButton()
        initializeErrorView()
        let views: [UIView] = [nameLabel, categoryLabel, closeButton, textView, imageView, actionButton, errorView]
        views.forEach(addSubview)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initializeView() {
        backgroundColor = .white
        layer.borderColor = Color.mainTheme.cgColor
        layer.borderWidth = 1
    }
    
    private func inintializeLabels() {
        nameLabel.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight(1))
        categoryLabel.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight(0.1))
    }
    
    private func initializeCloseButton() {
        closeButton.setImage(Images.close?.withRenderingMode(.alwaysTemplate), for: .normal)
        closeButton.tintColor = Color.mainTheme
    }
    
    private func initializeTextView() {
        textView.isScrollEnabled = true
        textView.isEditable = false
    }
    
    private func initializeImageView() {
        imageView.contentMode = .scaleAspectFit
    }
    
    private func initializeActionButton() {
        actionButton.imageView?.contentMode = .scaleAspectFit
        actionButton.imageEdgeInsets.top = 5
        actionButton.imageEdgeInsets.bottom = 5
        actionButton.isHidden = true
    }
    
    private func initializeErrorView() {
        errorView.isHidden = true
    }

    override func layoutSubviews() {
        updateCloseButtonFrames()
        updateLabelFrames()
        FavoriteButtonFrames()
        updateImageViewFrame()
        updateTextViewFrame()
        updateErrorViewFrame()
    }
    
    private func updateCloseButtonFrames() {
        let side: CGFloat = 20
        closeButton.frame = CGRect(x: bounds.maxX - side - padding, y: padding, width: side, height: side)
    }
    
    private func updateLabelFrames() {
        nameLabel.frame = CGRect(
            x: padding, y: padding, width: bounds.maxX - padding*3 - closeButton.frame.width, height: 30)
        categoryLabel.frame = CGRect(
            x: padding, y: nameLabel.frame.maxY, width: bounds.maxX - padding, height: 15)
    }
    
    private func updateTextViewFrame() {
        textView.frame = CGRect(
            x: 0, y: categoryLabel.frame.maxY + padding, width: imageView.frame.minX - padding, height: imageView.frame.height)
    }
    
    private func updateImageViewFrame() {
        let side = actionButton.frame.minY - categoryLabel.frame.maxY - padding*2
        imageView.frame = CGRect(
            x: bounds.maxX - side - padding, y: categoryLabel.frame.maxY + padding, width: side, height: side)
    }
    
    private func FavoriteButtonFrames() {
        let height: CGFloat = 40
        let width: CGFloat = 50
        actionButton.frame = CGRect(x: bounds.midX - width/2, y: bounds.maxY - height, width: width, height: height)
    }
    
    private func updateErrorViewFrame() {
        let y = categoryLabel.frame.maxY
        errorView.frame = CGRect(x: padding, y: y + padding, width: bounds.width - padding*2, height: bounds.maxY - y)
    }
    
    public func showError(with text: String) {
        errorView.label.text = text
        errorView.isHidden = false
    }
}
