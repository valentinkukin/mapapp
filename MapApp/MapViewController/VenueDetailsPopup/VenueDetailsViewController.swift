//
//  VenueDetailsViewController.swift
//  MapApp
//
//  Created by Kukin Valentin on 10/22/18.
//  Copyright © 2018 MobiDev. All rights reserved.
//

import UIKit
import Models
import Foursquer
import Constants
import AlamofireImage

protocol VenueDetailsViewControllerDelegate: class {
    func venueDetailsViewControllerDidTapOnCloseButton(_ viewController: VenueDetailsViewController)
    func venueDetailsViewControllerDidTapOnActionButton(_ viewController: VenueDetailsViewController)
}

class VenueDetailsViewController: UIViewController {

    private let actionButtonIcon: UIImage?
    private(set) var venue: Venue
    private var detailsView: VenueDetailsView? {
        return view as? VenueDetailsView
    }
    weak var delegate: VenueDetailsViewControllerDelegate?

    init(venue: Venue, actionButtonIcon: UIImage?) {
        self.venue = venue
        self.actionButtonIcon = actionButtonIcon
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = VenueDetailsView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setActionButtonIcon(actionButtonIcon)
        updateLabels()
        addButtonTargets()
        requestVenueDetails()
    }
    
    public func setActionButtonIcon(_ image: UIImage?) {
        detailsView?.actionButton.setImage(image, for: .normal)
    }
    
    private func updateLabels() {
        detailsView?.nameLabel.text = venue.name
        detailsView?.categoryLabel.text = venue.category
    }
    
    private func addButtonTargets() {
        detailsView?.closeButton.addTarget(self, action: #selector(handleClose(_:)), for: .touchUpInside)
        detailsView?.actionButton.addTarget(self, action: #selector(handleAction(_:)), for: .touchUpInside)
    }
    
    @objc
    private func handleClose(_ sender: UIButton) {
        delegate?.venueDetailsViewControllerDidTapOnCloseButton(self)
    }
    
    @objc
    private func handleAction(_ sender: UIButton) {
        delegate?.venueDetailsViewControllerDidTapOnActionButton(self)
    }
    
    private func requestVenueDetails() {
        let request = VenueRequestBuilder({return VenueRequest(endPoint: .details(venue.id), parser: VenueParser.venueDetail)})
            .make()
        request.send { [weak self] result in
            switch result {
            case let .value(venueDetail):
                self?.updateVenue(with: venueDetail)
                self?.updateView()
            case let .error(error):
                self?.detailsView?.showError(with: error.localizedDescription)
            }
        }
    }
    
    private func updateVenue(with venueDetail: VenueDetail) {
        venue.venueDescription = venueDetail.description
        venue.imageURL = venueDetail.imageURL
    }
    
    private func updateView() {
        detailsView?.actionButton.isHidden = false
        detailsView?.textView.text = venue.venueDescription
        guard let imageURL = venue.imageURL, let url = URL(string: imageURL) else {
            return
        }
        detailsView?.imageView.af_setImage(withURL: url)
    }
}
