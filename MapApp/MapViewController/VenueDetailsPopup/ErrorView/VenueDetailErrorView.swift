//
//  VenueDetailErrorView.swift
//  MapApp
//
//  Created by Kukin Valentin on 11/6/18.
//  Copyright © 2018 MobiDev. All rights reserved.
//

import UIKit

class VenueDetailsErrorView: UIView {
    
    let label = UILabel()
    
    init() {
        super.init(frame: .zero)
        initializeLabel()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initializeLabel() {
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight(0.1))
        addSubview(label)
    }
    
    override func layoutSubviews() {
        label.frame = bounds
    }
}
