//
//  MapViewModel.swift
//  MapApp
//
//  Created by Kukin Valentin on 11/5/18.
//  Copyright © 2018 MobiDev. All rights reserved.
//

import Foundation
import UIKit

struct MapViewModel {
    let title: String
    let canShowUserLocation: Bool
    let canShowSelectedLocation: Bool
    let rightBarButtonIcon: UIImage?
}
