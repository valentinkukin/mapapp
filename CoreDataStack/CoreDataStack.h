//
//  CoreDataStack.h
//  CoreDataStack
//
//  Created by Kukin Valentin on 10/22/18.
//  Copyright © 2018 MobiDev. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for CoreDataStack.
FOUNDATION_EXPORT double CoreDataStackVersionNumber;

//! Project version string for CoreDataStack.
FOUNDATION_EXPORT const unsigned char CoreDataStackVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CoreDataStack/PublicHeader.h>


