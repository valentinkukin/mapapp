//
//  Core.swift
//  MapApp
//
//  Created by Kukin Valentin on 11/2/18.
//  Copyright © 2018 MobiDev. All rights reserved.
//

import Foundation
import CoreData
import Models

public class CoreDataStack {
    
    public static let defaultStack = CoreDataStack()
    
    private let entityName = "Venue"
    
    private lazy var managedObjectContext: NSManagedObjectContext = {
        let managedObjectContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = persistentStoreCoordinator
        return managedObjectContext
    }()
    private lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        let persistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: getManagedObjectModel())
        return persistentStoreCoordinator
    }()
    
    private lazy var memoryStore: NSPersistentStore = createMemoryStore()
    private lazy var sqlStore: NSPersistentStore = createSQLStore()
    
    private(set) public lazy var fetchedResultsController: NSFetchedResultsController<Venue> = {
        let fetchRequest = NSFetchRequest<Venue>(entityName: entityName)
        let sortDescriptor = NSSortDescriptor(key: "name.firstLetter", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor, NSSortDescriptor(key: "name", ascending: true) ]
        fetchRequest.affectedStores = [sqlStore]
        let fetchedResultsController = NSFetchedResultsController<Venue>(fetchRequest: fetchRequest,
                                                                        managedObjectContext: managedObjectContext,
                                                                        sectionNameKeyPath: "name.firstLetter", cacheName: nil)
        do {
            try fetchedResultsController.performFetch()
        }catch let error {
            print(error)
        }
        
        return fetchedResultsController
    }()
    
    init() {
    }
    
    public func createVenue(id: String, name: String, category: String, latitude: Double, longitude: Double,
        description: String? = nil, imageURL: String? = nil) -> Venue {
        let venue = Venue(context: managedObjectContext)
        venue.id = id
        venue.name = name
        venue.category = category
        venue.latitude = latitude
        venue.longitude = longitude
        venue.venueDescription = description
        venue.imageURL = imageURL
        managedObjectContext.assign(venue, to: memoryStore)
        saveContext()
        return venue
    }
    
    public func clearAllInMemoryVenues() {
        do {
            try persistentStoreCoordinator.remove(memoryStore)
            memoryStore = createMemoryStore()
        } catch let error {
            print(error)
        }
    }

    public func delete(_ venue: Venue) {
        managedObjectContext.delete(venue)
        saveContext()
    }
    
    public func save(_ venue: Venue) {
        managedObjectContext.perform { [weak self] in
            guard let sSelf = self else {
                return
            }
            let favoriteVenue = Venue(context: sSelf.managedObjectContext)
            favoriteVenue.copy(venue)
            sSelf.managedObjectContext.assign(favoriteVenue, to: sSelf.sqlStore)
            sSelf.saveContext()
        }
    }
    
    private func saveContext() {
        managedObjectContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch let error{
                print(error)
            }
        }
    }
    
    private func createSQLStore() -> NSPersistentStore {
        let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last!
        let venuesInfoStoreURL = url.appendingPathComponent("Venue.sqlite")
        do {
            let sqlStore = try persistentStoreCoordinator.addPersistentStore(
                ofType: NSSQLiteStoreType,
                configurationName: nil,
                at: venuesInfoStoreURL,
                options: nil)
            return sqlStore
        } catch {
            fatalError("Couldn't create PersistentStores, please check your code")
        }
    }
    
    private func createMemoryStore() -> NSPersistentStore {
        do {
            let memoryStore = try persistentStoreCoordinator.addPersistentStore(
                ofType: NSInMemoryStoreType,
                configurationName: nil,
                at: nil,
                options: nil)
            return memoryStore
        } catch {
            fatalError("Couldn't create PersistentStores, please check your code")
        }
    }
    
    private func getManagedObjectModel() -> NSManagedObjectModel {
        guard let modelURL = Bundle(for: Venue.self).url(forResource: "VenuesModel", withExtension: "momd") else {
            fatalError("Couldn't open momd file, please check your code")
        }
        guard let managedObjectModel = NSManagedObjectModel(contentsOf: modelURL) else {
            fatalError("Couldn't create NSManagedObjectModel, please check your code")
        }
        return managedObjectModel
    }
}

public extension NSString {
    
    @objc func firstLetter() -> String {
        let initial = substring(to: 1)
        return initial
    }
}
